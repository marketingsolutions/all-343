window.onload = function() {
    let start = Date.now();

    logo.style.bottom = '80%';
    smallBall.style.bottom = '950px';
    smallBallTranstarent.style.bottom = '950px';
    bigBallTranstarent.style.bottom = '950px';
    bigBall.style.bottom = '1150px';
    present.style.bottom = '1150px';

    let timerTwo = setInterval(function() {
        let timePassed = Date.now() - start;
        // инверсия цвета лого
        if ( bigBall.getBoundingClientRect().y < 100 && bigBall.getBoundingClientRect().y > -150) {
            logo.style.filter = 'brightness(0) invert(1)';

            presentImg.style.transform = 'rotate(15deg)';

            setTimeout(function() {
                presentImg.style.transform = 'rotate(-15deg)';
            }, 2000);
        } else {
            logo.style.filter = 'none';
        }
        // анимация подарка
        if (bigBall.getBoundingClientRect().y < -512) {
            present.style.transitionDuration = '2s';
            present.style.bottom = '26%';
            presentImg.style.transform = 'rotate(0)';
            if (window.innerWidth > 700) {
                presentImg.style.width = '80%';
            } else {
                presentImg.style.width = '100%';
            }

            setTimeout( function () {
                buttonLink.style.display = 'block';
            }, 1000);
        }
        if (timePassed > 12000) clearInterval(timerTwo);
    }, 20);
}
